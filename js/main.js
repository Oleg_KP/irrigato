$(function(){
  $('#myCarousel').carousel({
    interval: 5000
  });

  $('#carousel-text').html($('#slide-content-0').html());

      //Handles the carousel thumbnails
      $('[id^=carousel-selector-]').click( function(){
        var id = this.id.substr(this.id.lastIndexOf("-") + 1);
        var id = parseInt(id);
        $('#myCarousel').carousel(id);
      });


      // When the carousel slides, auto update the text
      $('#myCarousel').on('slid.bs.carousel', function (e) {
       var id = $('.item.active').data('slide-number');
       $('#carousel-text').html($('#slide-content-'+id).html());
     });
      $('#media').carousel({
        pause: true,
        interval: false,
      });

/*      $('.acordeon .wrap .head').click(function(){        
        $(this).closest('.wrap').toggleClass('open');
          $('.acordeon .wrap').each(function(){            
              if($(this).hasClass('open')){
                $(this).find('.content').slideDown(500);
              }
              else
                {$(this).find('.content').slideUp(500);
                }
        $(this).closest('.wrap').removeClass('open');
        });
      });*/

      $('.acordeon .wrap .content').hide();
      $('.acordeon .wrap .head').click(function(){
        if($(this).next('.content').is(':visible')){
            $(this).next('.content').slideUp(500);
        }
        else{
          $(this).closest('.acordeon').find('.content').slideUp(500);
          $(this).next('.content').slideDown(500);
        }
        $('.acordeon .wrap .content').each(function(){
          if ($(this).is(':visible')) {
            $(this).closest('.wrap').toggleClass('open');
          }
        });
      });    
    })	

